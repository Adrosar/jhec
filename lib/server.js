#!/usr/bin/env node

// Moduły:
const path = require('path');
const express = require('express');
const serveIndex = require('serve-index')

// Katalogi:
const dir = require('./directories.js');

const staticOptions = {
    etag: true,
    maxAge: 60
};

const app = express();

function serverBuild(port) {

    app.use('/source.bundle.js', function (req, res, next) {
        const fileName = path.resolve(dir.build, 'source.bundle.js');
        res.sendFile(fileName);
    });

    app.use('/test.bundle.js', function (req, res, next) {
        const fileName = path.resolve(dir.build, 'test.bundle.js');
        res.sendFile(fileName);
    });

    app.use('/', express.static(dir.web, staticOptions));
    app.use('/', serveIndex(dir.web, {
        'icons': true
    }));

    app.listen(port, function () {
        console.log('Example app listening on port ' + port + ' !');
    })
}

serverBuild(61188);