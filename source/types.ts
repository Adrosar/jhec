
export type JecOptions = {
    document: Document;
}

export type JTagName = keyof HTMLElementTagNameMap;

export type JClassList = Array<string>;

export type JOptions = {
    handle?: string;
    class?: JClassList;
    attrs?: Array<[string, string]>;
    style?: JDict<string>;
    text?: string;
}

export type JChildren = Array<JTML>;

// JSON HTML => JHML
export type JTML = {
    [0]: JTagName;
    [1]?: JOptions;
    [2]?: JChildren;
}

export type JDict<T> = {
    [K: string]: T;
}