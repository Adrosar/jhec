import { Private } from "priv-fields";
import { JTML, JTagName, JOptions, JChildren, JDict, JecOptions } from "./types";

function _getWindow(): Window {
    if (!!window) {
        return window;
    }

    throw '2zwKLJ';
}

function _getDocument(ref: JEC): Document {

    const document = _(ref).document;

    if (typeof document === "object") {
        return document;
    }

    return _getWindow().document;
}

function _createElement<K extends keyof HTMLElementTagNameMap>(ref: JEC, tagName: K): HTMLElementTagNameMap[K] {
    return _getDocument(ref).createElement(tagName);
}

function _createText(ref: JEC, text: string): Text {
    return _getDocument(ref).createTextNode(text);
}

function _createOneElement(ref: JEC, jtml: JTML, result: any) {

    const tagName: JTagName | undefined = jtml[0];
    const options: JOptions | undefined = jtml[1];
    const children: JChildren | undefined = jtml[2];

    const element = _createElement(ref, tagName);

    if (options && options.text) {
        element.appendChild(_createText(ref, options.text));
    }

    if (options && options.handle) {
        result[options.handle] = element;
    }

    if (options && options.class) {
        element.className = options.class.join(" ");
    }

    if (options && options.attrs) {
        for (let i = 0; i < options.attrs.length; i++) {
            const key: string = options.attrs[i][0];
            const val: string = options.attrs[i][1];
            element.setAttribute(key, val);
        }
    }

    if (options && options.style) {
        for (const key in options.style) {
            if (options.style.hasOwnProperty(key)) {
                (<any>element.style)[key] = options.style[key];
            }
        }
    }

    if (children && children.length > 0) {
        for (let i = 0; i < children.length; i++) {
            const childJTML: JTML = children[i];
            const childElement = _createOneElement(ref, childJTML, result);
            element.appendChild(childElement);
        }
    }

    return element;
}

type Priv = {
    document?: Document;
}

const priv = new Private<JEC, Priv>();
const _ = priv.createAccess();

// Json Element Constructor
export class JEC {

    public element: HTMLElement;
    public input: JTML;
    public output: JDict<HTMLElement>;

    constructor(jtml: JTML, options?: JecOptions) {

        priv.initValue(this, {
            document: undefined
        });

        if (options && options.document) {
            _(this).document = options.document;
        }

        const result: JDict<HTMLElement> = {};
        this.element = _createOneElement(this, jtml, result);
        this.input = jtml;
        this.output = result;
    }
}