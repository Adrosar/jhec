
# JSON HTMLElement Creator

The module is used to convert JTML syntax to an HTMLElement object. JTML syntax is a structure similar to HTML but saved in the form of a JSON object.

## Installation

From  [npmjs.com](https://www.npmjs.com) _(recommended)_ - choose one of the options:

 - `npm install jhec`
 - `yarn add jhec`

From [Adrosar/jhec](https://bitbucket.org/Adrosar/jhec) - downloads the entire contents of the repository:
```
npm install bitbucket:Adrosar/jhec
```

## How to use

```typescript
import { JEC, JTML } from 'jhec';

const jtml: JTML = ['div', { handle: 'root', class: ['foo', 'foo-root', 'active'] }, [
    ['div', { class: ['foo', 'foo-label'] }, [
        ['span', { handle: 'label', class: ['foo', 'foo-label-text'], text: "Foo" }]
    ]],
    ['div', { handle: 'btnOK', text: "[OK]", attrs: [['data-id', 'OK-123']], style: { color: 'red' } }]
]];

const jec:JEC = new JEC(jtml);

console.log(jec.input);// input === jtml
console.log(jec.element);// HTMLElemen
console.log(jec.output);// Object:

/* jec.output (types):
{
    btnOK: HTMLDivElement,
    label: HTMLSpanElement,
    root: HTMLDivElement,
}
*/
```

## Development

### Running the tests:

For Node.js:
```
npm run test:node
```

For browser:

 1. Run in terminal `npm run test:browser`
 2. Run in terminal `npm run server`
 3. Open link http://127.0.0.1:61188/test.html
 4. Open DevTools Console _(**F12** in Google Chrome - Windows)_
 5. Use **F5** to refresh the page
 6. If there is no exception, then the test passed successfully


## Versioning

I use the versioning system [SemVer](http://semver.org/) _(2.0.0)_

## Author

* **Adrian Gargula**

## License

This project is licensed under the ISC License - see the [wiki/ISC_license](https://en.wikipedia.org/wiki/ISC_license)

## Other

This project is based on [Adrosar/ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit) [_(version 1.5.0)_](https://bitbucket.org/Adrosar/ts-startek-kit/src/1.5.0/)
