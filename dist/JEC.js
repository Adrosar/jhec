"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const priv_fields_1 = require("priv-fields");
function _getWindow() {
    if (!!window) {
        return window;
    }
    throw '2zwKLJ';
}
function _getDocument(ref) {
    const document = _(ref).document;
    if (typeof document === "object") {
        return document;
    }
    return _getWindow().document;
}
function _createElement(ref, tagName) {
    return _getDocument(ref).createElement(tagName);
}
function _createText(ref, text) {
    return _getDocument(ref).createTextNode(text);
}
function _createOneElement(ref, jtml, result) {
    const tagName = jtml[0];
    const options = jtml[1];
    const children = jtml[2];
    const element = _createElement(ref, tagName);
    if (options && options.text) {
        element.appendChild(_createText(ref, options.text));
    }
    if (options && options.handle) {
        result[options.handle] = element;
    }
    if (options && options.class) {
        element.className = options.class.join(" ");
    }
    if (options && options.attrs) {
        for (let i = 0; i < options.attrs.length; i++) {
            const key = options.attrs[i][0];
            const val = options.attrs[i][1];
            element.setAttribute(key, val);
        }
    }
    if (options && options.style) {
        for (const key in options.style) {
            if (options.style.hasOwnProperty(key)) {
                element.style[key] = options.style[key];
            }
        }
    }
    if (children && children.length > 0) {
        for (let i = 0; i < children.length; i++) {
            const childJTML = children[i];
            const childElement = _createOneElement(ref, childJTML, result);
            element.appendChild(childElement);
        }
    }
    return element;
}
const priv = new priv_fields_1.Private();
const _ = priv.createAccess();
// Json Element Constructor
class JEC {
    constructor(jtml, options) {
        priv.initValue(this, {
            document: undefined
        });
        if (options && options.document) {
            _(this).document = options.document;
        }
        const result = {};
        this.element = _createOneElement(this, jtml, result);
        this.input = jtml;
        this.output = result;
    }
}
exports.JEC = JEC;
