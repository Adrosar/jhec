export declare type JecOptions = {
    document: Document;
};
export declare type JTagName = keyof HTMLElementTagNameMap;
export declare type JClassList = Array<string>;
export declare type JOptions = {
    handle?: string;
    class?: JClassList;
    attrs?: Array<[string, string]>;
    style?: JDict<string>;
    text?: string;
};
export declare type JChildren = Array<JTML>;
export declare type JTML = {
    [0]: JTagName;
    [1]?: JOptions;
    [2]?: JChildren;
};
export declare type JDict<T> = {
    [K: string]: T;
};
