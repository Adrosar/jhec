import { JTML, JDict, JecOptions } from "./types";
export declare class JEC {
    element: HTMLElement;
    input: JTML;
    output: JDict<HTMLElement>;
    constructor(jtml: JTML, options?: JecOptions);
}
