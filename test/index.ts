import { JEC, JTML } from "../source";

const jtml: JTML = ['div', { handle: 'root', class: ['foo', 'foo-root', 'active'] }, [
    ['div', { class: ['foo', 'foo-label'] }, [
        ['span', { handle: 'label', class: ['foo', 'foo-label-text'], text: "Foo" }]
    ]],
    ['div', { handle: 'btnOK', text: "[OK]", attrs: [['data-id', 'OK-123']], style: { color: 'red' } }]
]];

var jec: JEC;

if (typeof process === "object" && typeof __filename === "string") {
    const jsdom = require('jsdom');
    const dom = new jsdom.JSDOM(`<!DOCTYPE html></html>`);

    jec = new JEC(jtml, {
        document: dom.window.document
    });

} else {
    jec = new JEC(jtml);
}

if (typeof jec !== "object") {
    throw new Error();
}

if (typeof jec.element !== "object") {
    throw new Error();
}

if (!(jec.input instanceof Array)) {
    throw new Error();
}

if (typeof jec.output !== "object") {
    throw new Error();
}

if (typeof jec.output['root'] !== "object") {
    throw new Error();
}

if (typeof jec.output['label'] !== "object") {
    throw new Error();
}

if (typeof jec.output['btnOK'] !== "object") {
    throw new Error();
}

if (jec.output['btnOK'].getAttribute('data-id') !== "OK-123") {
    throw new Error();
}

if (typeof jec.output['btnOK'].style !== "object") {
    throw new Error();
}

if (jec.output['btnOK'].style.color !== "red") {
    throw new Error();
}

if (jec.output['label'].innerHTML !== "Foo") {
    throw new Error();
}

if (jec.output['root'].className !== "foo foo-root active") {
    throw new Error();
}

if (jec.output['label'].className !== "foo foo-label-text") {
    throw new Error();
}

if (jec.element.innerHTML !== `<div class="foo foo-label"><span class="foo foo-label-text">Foo</span></div><div data-id="OK-123" style="color: red;">[OK]</div>`) {
    throw new Error();
}